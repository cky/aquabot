# SPDX-FileCopyrightText: © 2021 Chris Jester-Young <cky@cky.nz>
# SPDX-License-Identifier: AGPL-3.0-or-later

module Net
  type port = Integer | String

  interface _Consumer
    def <<: (String) -> void
  end

  class HTTP
    type header = Hash[String, String]

    class Get < HTTPRequest
    end

    def self.get_response: (String host, String path, ?port)
                            ?{ (HTTPResponse) -> void } -> HTTPResponse |
                           (URI::Generic, ?header)
                            ?{ (HTTPResponse) -> void } -> HTTPResponse

    def self.start: (String address, ?Integer | String port, ?String p_addr,
                     ?Integer | String p_port, ?String p_user, ?String p_pass,
                     **untyped) -> HTTP |
                    [R] (String address, ?Integer | String port, ?String p_addr,
                         ?Integer | String p_port, ?String p_user, ?String p_pass,
                         **untyped) { (HTTP) -> R } -> R

    def request: (HTTPRequest, ?String body) ?{ (HTTPResponse) -> void } -> HTTPResponse
  end

  module HTTPHeader
    def content_type: -> String?
  end

  class HTTPRequest
    def initialize: (URI::Generic | String path, ?Hash[String, String] initheader) -> void
  end

  class HTTPResponse
    include HTTPHeader

    attr_accessor body: String

    def value: -> void
    def read_body: -> String | [T] (T & _Consumer) -> T | { (String) -> void } -> void
  end

  class HTTPSuccess < HTTPResponse
  end
end
