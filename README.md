## AquaBot

AquaBot is a bot I wrote for Aquarius, a fellow guildy in Almost Epic,
the Gems of War guild that I'm in. She was super sad when Mantaro's
`cat` command stopped working, and after months of it not being fixed,
I decided it was time to fix the problem myself.

AquaBot is designed to respond to emoji commands only (and all have to
be prefixed with ♒). That way, there is no chance of collision with
other bots' commands.

This is my first Discord bot, so I appreciate any feedback experienced
[discordrb] users may have about the code. I'm not new to Ruby, so
hopefully the code is still easy to work with despite my being new to
discordrb.

### Installation and usage

Copy `config/config.rb.editme` to `config/config.rb` and set suitable
values for the bot token, client ID, and channels. You should (after
running Bundler, of course) be able to run `bin/start` and have
everything just work.

Tested on:

+ Ruby 3.0.2 on Debian 10.10.

This code has only been tested on Linux and macOS. In particular, it
won't work well on Windows; my code depends on being able to delete
open files, something Windows doesn't like at all.

### Author

[Chris Jester-Young]

[discordrb]: https://github.com/meew0/discordrb
[Chris Jester-Young]: https://gitlab.com/cky
