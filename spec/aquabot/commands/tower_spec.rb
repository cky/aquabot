# frozen_string_literal: true

# SPDX-FileCopyrightText: © 2021 Chris Jester-Young <cky@cky.nz>
# SPDX-License-Identifier: AGPL-3.0-or-later

require 'discordrb'
require_relative '../../../lib/aquabot/commands/tower'

RSpec.describe Aquabot::Commands::Tower do
  describe '.commands' do
    subject(:commands) { described_class.commands }

    let(:expected_attributes) do
      {
        keys: a_collection_containing_exactly(:🏰, :🏯)
      }
    end

    it { is_expected.to have_attributes(expected_attributes) }
  end

  describe '.commands[:🏰]' do
    subject(:command) { described_class.commands[:🏰] }

    let(:expected_attributes) do
      {
        name: :🏰,
        attributes: a_hash_including(aliases: %i[🏯])
      }
    end

    it { is_expected.to be_a(Discordrb::Commands::Command) }
    it { is_expected.to have_attributes(expected_attributes) }
  end

  describe '.commands[:🏯]' do
    subject(:command) { described_class.commands[:🏯] }

    let(:expected_attributes) do
      {
        name: :🏯,
        aliased_command: an_object_equal_to(described_class.commands[:🏰])
      }
    end

    it { is_expected.to be_a(Discordrb::Commands::CommandAlias) }
    it { is_expected.to have_attributes(expected_attributes) }
  end

  describe 'dispatcher' do
    let(:passthrough_messages) do
      %i[canonical_room! canonical_sunglasses canonical_type canonical_type! rooms]
    end

    let(:dispatcher) do
      class_double(described_class).tap do |double|
        passthrough_messages.each do |message|
          allow(double).to receive(message, &described_class.method(message))
        end
      end
    end

    let(:block) { described_class.commands[:🏰].instance_variable_get(:@block) }
    let(:event) { instance_double(Discordrb::Commands::CommandEvent) }

    def dispatch(line)
      # Actual input normalisation happens in Aquabot::Bot#simple_execute;
      # this is a simplified version to enable tests to work.
      line = line.delete_prefix('♒🏰').gsub(/\s+/, '')
      dispatcher.instance_exec(event, line, &block)
    end

    shared_examples 'dispatch' do |line, method, *args|
      context "with input `#{line}`" do
        specify do
          expect(dispatcher).to receive(method).with(anything, *args)
          dispatch(line)
        end
      end
    end

    shared_examples 'invalid' do |line|
      context "with input `#{line}`" do
        it 'is expected to reject the request' do
          expect(dispatch(line)).to eq('Invalid command; enter ♒🏰❓ for help')
        end
      end
    end

    include_examples 'dispatch', '♒🏰help', :show_help
    include_examples 'dispatch', '♒🏰❓', :show_help
    include_examples 'dispatch', '♒🏰❔', :show_help

    # By default, the map is shown with boon rooms (armour, life, attack,
    # magic, and power) hidden. If you use ⚡, then power is not hidden.
    # If you use 😎, then nothing is hidden.
    include_examples 'dispatch', '♒🏰', :show_map, nil
    include_examples 'dispatch', '♒🏰😎', :show_map, '😎'
    include_examples 'dispatch', '♒🏰⚡', :show_map, '⚡'

    include_examples 'dispatch', '♒🏰1', :show_floor, 1, nil
    include_examples 'dispatch', '♒🏰😎2', :show_floor, 2, '😎'
    include_examples 'dispatch', '♒🏰⚡3', :show_floor, 3, '⚡'

    include_examples 'dispatch', '♒🏰4ii', :show_room, 4, 'ii'
    include_examples 'dispatch', '♒🏰5iii', :show_room, 5, 'iii'
    include_examples 'dispatch', '♒🏰6iv', :show_room, 6, 'iv'
    include_examples 'dispatch', '♒🏰7v', :show_room, 7, 'v'
    include_examples 'dispatch', '♒🏰8boss', :show_room, 8, 'boss'

    include_examples 'dispatch', '♒🏰9iiar', :set_room_types, 9, 'ii' => 'armor'
    include_examples 'dispatch', '♒🏰10iiili', :set_room_types, 10, 'iii' => 'life'
    include_examples 'dispatch', '♒🏰11ivat', :set_room_types, 11, 'iv' => 'attack'
    include_examples 'dispatch', '♒🏰12vma', :set_room_types, 12, 'v' => 'magic'
    include_examples 'dispatch', '♒🏰13bosspo', :set_room_types, 13, 'boss' => 'power'

    # You can specify all the scrolls on a floor at once.
    include_examples 'dispatch', '♒🏰14arliatma', :set_room_types, 14,
                     'ii' => 'armor', 'iii' => 'life', 'iv' => 'attack', 'v' => 'magic'
    # For an upper floor (26 or higher), you can specify the boss room too.
    include_examples 'dispatch', '♒🏰26haluunfihe', :set_room_types, 26,
                     'ii' => 'haste', 'iii' => 'luck', 'iv' => 'unlock',
                     'v' => 'fireball', 'boss' => 'heroism'
    # A lower floor always skips the boss room, even if specified.
    include_examples 'dispatch', '♒🏰25haluunfihe', :set_room_types, 25,
                     'ii' => 'haste', 'iii' => 'luck', 'iv' => 'unlock',
                     'v' => 'fireball'

    # You must specify 4 or 5 rooms, but you can use -- to skip rooms.
    # Skipping a room does not reset it.
    include_examples 'invalid', '♒🏰15arhaun'
    include_examples 'dispatch', '♒🏰16ar--haun', :set_room_types, 16,
                     'ii' => 'armor', 'iv' => 'haste', 'v' => 'unlock'

    # You can reset a whole floor. You can't reset an individual room.
    # You also can't reset multiple floors at once.
    include_examples 'dispatch', '♒🏰17reset', :reset_floor, 17

    # You can specify the scroll type in full.
    include_examples 'dispatch', '♒🏰18iiarmor', :set_room_types, 18, 'ii' => 'armor'
    # "Armour" is also an accepted spelling for "armor".
    include_examples 'dispatch', '♒🏰19iiiarmour', :set_room_types, 19, 'iii' => 'armor'
    # Scroll names must be either in full or just the first two letters.
    # Other abbreviations are not accepted.
    include_examples 'invalid', '♒🏰20ivarm'
    # You can use the long form for the bulk-input variant too (and you
    # can mix and match if you so wish). Note that the input is neither
    # case-sensitive nor space-sensitive, so enter these whichever way
    # is easiest for you.
    include_examples 'dispatch', '♒🏰21 AR Life Attack Magic', :set_room_types, 21,
                     'ii' => 'armor', 'iii' => 'life', 'iv' => 'attack', 'v' => 'magic'
  end
end
