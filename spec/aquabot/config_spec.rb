# frozen_string_literal: true

# SPDX-FileCopyrightText: © 2021 Chris Jester-Young <cky@cky.nz>
# SPDX-License-Identifier: AGPL-3.0-or-later

require_relative '../../lib/aquabot/config'

RSpec.describe Aquabot::Config do
  it { is_expected.to respond_to(:apply, :top_level) }

  describe '.top_level' do
    subject { described_class.top_level }

    it { is_expected.to be_a(Aquabot::Config::TopLevel) }
  end

  describe '.apply' do
    it 'calls the given block with .top_level as self' do
      expect(described_class.apply { self }).to equal(described_class.top_level)
    end
  end
end

RSpec.describe Aquabot::Config::TopLevel do
  subject(:top_level) { described_class.new }

  it { is_expected.to respond_to(:core, :defaults, :commands, :command) }

  describe '#core' do
    subject { top_level.core }

    it { is_expected.to be_a(Aquabot::Config::Core) }
  end

  describe '#defaults' do
    subject { top_level.defaults }

    it { is_expected.to be_a(Aquabot::Config::Options) }
  end

  describe '#commands' do
    subject { top_level.commands }

    it { is_expected.to be_a(Hash) }
  end

  describe '#command' do
    subject { top_level.command(:foo) }

    it { is_expected.to be_a(Aquabot::Config::CommandOptions) }
    it { is_expected.to equal(top_level.commands[:foo]) }
  end
end

RSpec.describe Aquabot::Config::Core do
  it { is_expected.to respond_to(:bot_token, :bot_token=, :client_id, :client_id=) }
end

RSpec.describe Aquabot::Config::Options do
  it { is_expected.to respond_to(:channels, :channels=, :rate_limit, :rate_limit=) }
end

RSpec.describe Aquabot::Config::CommandOptions do
  subject(:command_options) { described_class.new(defaults: defaults) }

  let(:defaults) do
    Aquabot::Config::Options.new(channels: [1, 2, 3], rate_limit: {delay: 1})
  end

  it { is_expected.to respond_to(:channels, :channels=, :rate_limit, :rate_limit=) }

  describe '#channels' do
    it 'delegates to defaults if unset' do
      expect(command_options.channels).to eq([1, 2, 3])
    end

    it 'does not delegate to defaults if set' do
      command_options.channels = [4, 5, 6]
      expect(command_options.channels).to eq([4, 5, 6])
    end

    it 'uses the current value of defaults' do
      command_options
      defaults.channels = [7, 8, 9]
      expect(command_options.channels).to eq([7, 8, 9])
    end
  end

  describe '#rate_limit' do
    it 'delegates to defaults if unset' do
      expect(command_options.rate_limit).to eq(delay: 1)
    end

    it 'does not delegate to defaults if set' do
      command_options.rate_limit = {}
      expect(command_options.rate_limit).to eq({})
    end

    it 'uses the current value of defaults' do
      command_options
      defaults.rate_limit = {limit: 100, time_span: 60}
      expect(command_options.rate_limit).to eq(limit: 100, time_span: 60)
    end
  end
end
