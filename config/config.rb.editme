# frozen_string_literal: true

# SPDX-FileCopyrightText: © 2018, 2019, 2021 Chris Jester-Young <cky@cky.nz>
# SPDX-License-Identifier: CC0-1.0

require_relative '../lib/aquabot/config'

Aquabot::Config.apply do
  # @type self: Aquabot::Config::TopLevel

  # Put in the bot's token and client ID.
  core.bot_token = ''
  core.client_id = 0

  # Specify channels you want this bot to be active in.
  defaults.channels = []
  # Specify channels where bot control commands can be used.
  command(:control).channels = []
  # Specify channels where Tower commands can be used.
  command(:tower).channels = []

  # Set up rate limiting parameters.
  command(:cat).rate_limit = {delay: 5}
  command(:tower).rate_limit = {delay: 1}
end
