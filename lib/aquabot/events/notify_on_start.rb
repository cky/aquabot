# frozen_string_literal: true

# SPDX-FileCopyrightText: © 2018, 2021 Chris Jester-Young <cky@cky.nz>
# SPDX-License-Identifier: AGPL-3.0-or-later

require 'discordrb/container'

module Aquabot
  module Events
    # Sends a message to all bot control channels notifying that the
    # bot has booted up.
    module NotifyOnStart
      extend Discordrb::EventContainer

      HANDLER = ready do |event|
        bot = Aquabot::Bot.cast!(event.bot)
        bot.remove_handler(HANDLER)
        bot.config.command(:control).channels&.each do |id|
          bot.channel(id)&.send_message('👋')
        end
      end
    end
  end
end
