# frozen_string_literal: true

# SPDX-FileCopyrightText: © 2021 Chris Jester-Young <cky@cky.nz>
# SPDX-License-Identifier: AGPL-3.0-or-later

module Aquabot
  module Typecheck
    # Typechecked wrapper for `IO.popen`.
    module Popen
      module_function

      def popen(...)
        IO.popen(...)
      end
    end
  end
end
