# frozen_string_literal: true

# SPDX-FileCopyrightText: © 2018, 2019, 2021 Chris Jester-Young <cky@cky.nz>
# SPDX-License-Identifier: AGPL-3.0-or-later

require 'discordrb/container'
require_relative 'events/notify_on_start'

module Aquabot
  # All modules defined inside this module are automatically included by
  # `Aquabot::Bot`.
  module Events
    extend Discordrb::EventContainer

    constants(false).each do |const|
      include!(const_get(const, false))
    end
  end
end
