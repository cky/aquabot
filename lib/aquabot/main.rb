# frozen_string_literal: true

# SPDX-FileCopyrightText: © 2018, 2019, 2020, 2021 Chris Jester-Young <cky@cky.nz>
# SPDX-License-Identifier: AGPL-3.0-or-later

require 'discordrb'
require_relative 'patches'
require_relative 'config'
require_relative 'bot'

module Aquabot
  # Main class for AquaBot.
  module Main
    def self.run(config: Aquabot::Config.top_level, async: false)
      bot = Aquabot::Bot.new(config: config)
      bot.run(async)
    end
  end
end
