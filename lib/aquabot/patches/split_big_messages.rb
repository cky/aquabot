# frozen_string_literal: true

# SPDX-FileCopyrightText: © 2019, 2021 Chris Jester-Young <cky@cky.nz>
# SPDX-License-Identifier: AGPL-3.0-or-later

module Aquabot
  module Patches
    # Allows messages to collect to over 2000 characters, by immediately
    # flushing any buffered text before going over 2000 characters.
    module SplitBigMessages
      prepend_features Discordrb::Events::MessageEvent

      # Number of code points? UTF-8 bytes? Something else?
      MAX_MESSAGE_LENGTH = 2000

      def <<(message)
        size = saved_lines.sum { |line| line.size + 1 }
        if size + message.size > MAX_MESSAGE_LENGTH
          send_message saved_message
          drain
        end
        saved_lines << message
        dirty!
        nil
      end

      def saved_message
        @saved_message ||= saved_lines.join("\n")
      end

      def drain
        saved_lines.clear
        dirty!
        nil
      end

      def drain_into(result)
        return if result.is_a?(Discordrb::Message)

        self << result if result
        saved_message.tap { drain }
      end

      private

      def saved_lines
        @saved_lines ||= []
      end

      def dirty!
        remove_instance_variable(:@saved_message) if instance_variable_defined?(:@saved_message)
      end
    end
  end
end
