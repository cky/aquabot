# frozen_string_literal: true

# SPDX-FileCopyrightText: © 2018, 2021 Chris Jester-Young <cky@cky.nz>
# SPDX-License-Identifier: AGPL-3.0-or-later

module Aquabot
  module Patches
    # When `Discordrb::Gateway#close` is explicitly called, such as via
    # `Discordrb::Bot#stop`, it is not helpful to display "The websocket
    # connection has closed: nil" messages. We intentionally wanted to
    # close the connection; it's not an error.
    module SuppressSpuriousErrorMessage
      prepend_features Discordrb::Gateway

      def handle_close(error)
        error && super
      end
    end
  end
end
