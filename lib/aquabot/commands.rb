# frozen_string_literal: true

# SPDX-FileCopyrightText: © 2018, 2019, 2021 Chris Jester-Young <cky@cky.nz>
# SPDX-License-Identifier: AGPL-3.0-or-later

require 'discordrb/commands/container'
require_relative 'commands/cat'
require_relative 'commands/control'
require_relative 'commands/help'
require_relative 'commands/tower'

module Aquabot
  # All modules defined inside this module are automatically included by
  # `Aquabot::Bot`.
  module Commands
    extend Discordrb::Commands::CommandContainer

    constants(false).each do |const|
      include!(const_get(const, false))
    end
  end
end
