# frozen_string_literal: true

# SPDX-FileCopyrightText: © 2021 Chris Jester-Young <cky@cky.nz>
# SPDX-License-Identifier: AGPL-3.0-or-later

require 'set'

module Aquabot
  # `Discordrb::Commands::CommandBot#channels?` does a linear lookup of
  # the channel filters. It's fine if your filter set is small, but if
  # it's not, it will be problematic.
  class ChannelFilter
    class << self
      def [](filters)
        filter_cache[filters]
      end

      private

      def filter_cache
        @filter_cache ||= Hash.new do |cache, filters|
          cache[filters.dup.freeze] = new(filters)
        end
      end
    end

    def initialize(filters)
      names, ids = filters.partition { |f| f.is_a?(String) }
      @ids = Set.new(ids) { |i| i.resolve_id }.freeze
      @names = Set.new(names) { |n| n.to_s.delete('#') }.freeze
    end

    def match?(channel)
      ids.include?(channel.resolve_id) || names.include?(channel.name)
    end

    private

    # @dynamic ids, names
    attr_reader :ids, :names
  end
end
