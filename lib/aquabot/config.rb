# frozen_string_literal: true

# SPDX-FileCopyrightText: © 2021 Chris Jester-Young <cky@cky.nz>
# SPDX-License-Identifier: AGPL-3.0-or-later

module Aquabot
  # Configuration schema for AquaBot. See config/config.rb.editme.
  module Config
    def self.apply(&block)
      top_level.instance_eval(&block)
    end

    def self.top_level
      @top_level ||= TopLevel.new
    end

    # Top-level configuration.
    class TopLevel
      # @dynamic core, defaults, commands
      attr_reader :core, :defaults, :commands

      def initialize
        @core = Core.new
        @defaults = Options.new
        @commands = Hash.new { |hash, key| hash[key] = CommandOptions.new(defaults: @defaults) }
      end

      def command(name)
        commands[name]
      end
    end

    # Core configuration parameters, not command-specific.
    Core = _ = Struct.new(:bot_token, :client_id, keyword_init: true) # rubocop:disable Naming/ConstantName

    # Potentially command-specific options.
    Options = _ = Struct.new(:channels, :rate_limit, keyword_init: true) # rubocop:disable Naming/ConstantName

    # Command-specific options, delegating to Aquabot::Config.defaults
    # if unset.
    class CommandOptions < Options
      def initialize(defaults:, channels: nil, rate_limit: nil)
        super(channels: channels, rate_limit: rate_limit)
        @defaults = defaults
      end

      def channels
        super || @defaults.channels
      end

      def rate_limit
        super || @defaults.rate_limit
      end
    end
  end
end
