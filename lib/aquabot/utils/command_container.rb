# frozen_string_literal: true

# SPDX-FileCopyrightText: © 2018, 2019, 2021 Chris Jester-Young <cky@cky.nz>
# SPDX-License-Identifier: AGPL-3.0-or-later

require 'discordrb/commands/container'
require_relative '../config'

module Aquabot
  module Utils
    # Extend discordrb's `CommandContainer` with support for default
    # command attributes.
    module CommandContainer
      include Discordrb::Commands::CommandContainer

      def setup(name, **kwargs)
        @defaults = {}
        options = Aquabot::Config.top_level.command(name)
        setup_channels(options.channels)
        setup_rate_limit(name, **options.rate_limit) if options.rate_limit
        defaults(**kwargs)
      end

      def setup_channels(channels)
        return unless channels

        defaults(channels: channels)
      end

      def setup_rate_limit(name, limit: nil, time_span: nil, delay: nil)
        if limit && time_span
          bucket(name, limit: limit, time_span: time_span, delay: delay)
        else
          bucket(name, delay: delay)
        end
        defaults(bucket: name)
      end

      # Sets the default attributes all subsequent `command` calls
      # will use.
      def defaults(**attrs)
        @defaults.merge!(attrs)
      end

      def command(name, **attributes)
        attributes = @defaults.merge(attributes)
        super
      end
    end
  end
end
