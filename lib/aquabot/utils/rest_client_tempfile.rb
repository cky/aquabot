# frozen_string_literal: true

# SPDX-FileCopyrightText: © 2018, 2019, 2021 Chris Jester-Young <cky@cky.nz>
# SPDX-License-Identifier: AGPL-3.0-or-later

require 'tempfile'

module Aquabot
  module Utils
    # Augments `Tempfile` with attributes used by `RestClient::Payload`.
    class RestClientTempfile < Tempfile
      # @dynamic content_type, content_type=, original_filename, original_filename=
      attr_accessor :content_type, :original_filename

      def initialize
        super
        unlink
        binmode
      end

      # On MRI, Tempfile delegates to File but does not inherit from it.
      # Discordrb's `MessageEvent#attach_file` method explicitly checks
      # for `is_a?(File)` so this makes the check work.
      def is_a?(mod)
        mod == File || super
      end
    end
  end
end
