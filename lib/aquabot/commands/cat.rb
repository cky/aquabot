# frozen_string_literal: true

# SPDX-FileCopyrightText: © 2018, 2019, 2021 Chris Jester-Young <cky@cky.nz>
# SPDX-License-Identifier: AGPL-3.0-or-later

require 'json'
require 'net/http'
require 'socket'
require_relative '../utils/command_container'
require_relative '../utils/rest_client_tempfile'

module Aquabot
  module Commands
    # Implements the `cat` bot command.
    module Cat
      extend Aquabot::Utils::CommandContainer

      setup :cat, max_args: 0

      command(:🐱,
              aliases: %i[😺 😸 😹 😻 😼 😽 🙀 😿 😾],
              description: 'Cat all the things!') do |event|
        uri = image_uri or next "Can't get cat image URL 😿"
        temp = download(uri) or next "Can't download cat image 😿"
        event.attach_file(temp)
      end

      class << self
        private

        REQUEST_URI = URI.parse('http://aws.random.cat/meow')
        REQUEST = Net::HTTP::Get.new(REQUEST_URI)
        private_constant :REQUEST_URI, :REQUEST

        def image_uri
          # aws.random.cat resolves to 2 IP addresses, one of which
          # does not work. So try both and see which sticks.
          Addrinfo.foreach('aws.random.cat', 'http', :UNSPEC, :STREAM) do |ai|
            res = Net::HTTP.start(ai.ip_address, ai.ip_port) do |http|
              http.request(REQUEST)
            end
            if res.is_a?(Net::HTTPSuccess)
              value = uri_from_json(res.body)
              return value if value
            end
          end
          nil
        end

        def uri_from_json(json)
          parsed = JSON.parse(json)
          parsed.is_a?(Hash) ? URI.parse(parsed['file']) : nil
        rescue JSON::ParserError, URI::InvalidURIError => e
          warn "Can't get cat image URI: #{e.message}"
        end

        def download(uri)
          warn "Downloading #{uri}"
          file = Aquabot::Utils::RestClientTempfile.new
          file.original_filename = uri.path&.sub(%r{.*/}, '')
          Net::HTTP.get_response(uri) do |res|
            res.value
            res.read_body(file)
            file.rewind
            file.content_type = res.content_type
          end
          file
        rescue StandardError => e
          warn "Can't download cat image: #{e.message}"
        end
      end
    end
  end
end
