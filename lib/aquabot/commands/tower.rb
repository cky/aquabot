# frozen_string_literal: true

# SPDX-FileCopyrightText: © 2019, 2020, 2021 Chris Jester-Young <cky@cky.nz>
# SPDX-License-Identifier: AGPL-3.0-or-later

require 'redis'
require_relative '../utils/command_container'

module Aquabot
  module Commands
    # Implements the `tower` bot command.
    module Tower
      extend Aquabot::Utils::CommandContainer

      FLOOR = /[1-9]\d*/
      ROOM = /boss|iii?|i?v/i
      TYPE = /ar(?:mou?r)?|li(?:fe)?|at(?:tack)?|ha(?:ste)?|ma(?:gic)?|lu(?:ck)?|po(?:wer)?|un(?:lock)?|he(?:roism)?|fi(?:reball)?/i # rubocop:disable Layout/LineLength
      OPT_TYPE = /#{TYPE}|--/
      SUNGLASSES = /[😎⚡]/

      setup :tower

      command(:🏰,
              aliases: %i[🏯],
              description: 'Tower of Doom coordination') do |event, line = ''|
        case line
        when /\A(?<floor>#{FLOOR})(?<ii>#{OPT_TYPE})(?<iii>#{OPT_TYPE})(?<iv>#{OPT_TYPE})(?<v>#{OPT_TYPE})(?<boss>#{OPT_TYPE})?\z/o # rubocop:disable Layout/LineLength
          floor = Regexp.last_match(:floor).to_i
          # @type var types: Hash[room, type_]
          types = _ = rooms(floor).to_h { |x| [x, canonical_type(Regexp.last_match(x))] }.compact
          set_room_types(event, floor, types)
        when /\A(?<floor>#{FLOOR})(?<room>#{ROOM})(?<type>#{TYPE})\z/o
          floor = Regexp.last_match(:floor).to_i
          room = canonical_room!(Regexp.last_match(:room))
          type = canonical_type!(Regexp.last_match(:type))
          set_room_types(event, floor, room => type)
        when /\A(?<floor>#{FLOOR})(?<room>#{ROOM})\z/o
          floor = Regexp.last_match(:floor).to_i
          room = canonical_room!(Regexp.last_match(:room))
          show_room(event, floor, room)
        when /\A(?<floor>#{FLOOR})reset\z/o
          floor = Regexp.last_match(:floor).to_i
          reset_floor(event, floor)
        when /\A(?<sunglasses>#{SUNGLASSES})?(?<floor>#{FLOOR})\z/o
          floor = Regexp.last_match(:floor).to_i
          show_floor(event, floor, canonical_sunglasses(Regexp.last_match(:sunglasses)))
        when /\A(?<sunglasses>#{SUNGLASSES})?\z/o
          show_map(event, canonical_sunglasses(Regexp.last_match(:sunglasses)))
        when 'help', '❓', '❔'
          show_help(event)
        else
          next 'Invalid command; enter ♒🏰❓ for help'
        end
      end

      class << self
        private

        def redis
          @redis ||= Redis.new
        end

        def end_of_week
          now = Time.now.utc - (7 * 3600)
          day_offset = ((7 - now.wday) % 7) + 1
          rough_end = Time.utc(now.year, now.month, now.mday) + (day_offset * 86401)
          Time.utc(rough_end.year, rough_end.month, rough_end.mday, 7)
        end

        def key_for(channel_id, floor)
          "tower:#{channel_id}:#{floor}"
        end

        def rooms(floor)
          floor <= 25 ? %w[ii iii iv v] : %w[ii iii iv v boss]
        end

        def canonical_room!(room)
          case room
          when /\Aii\z/i then 'ii'
          when /\Aiii\z/i then 'iii'
          when /\Aiv\z/i then 'iv'
          when /\Av\z/i then 'v'
          when /\Aboss\z/i then 'boss'
          else raise ArgumentError, "Invalid room: #{room}"
          end
        end

        def room_name(room)
          room == 'boss' ? 'Boss' : room.upcase
        end

        def canonical_type(type)
          case type
          when /\Aar/i then 'armor'
          when /\Ali/i then 'life'
          when /\Aat/i then 'attack'
          when /\Aha/i then 'haste'
          when /\Ama/i then 'magic'
          when /\Alu/i then 'luck'
          when /\Apo/i then 'power'
          when /\Aun/i then 'unlock'
          when /\Ahe/i then 'heroism'
          when /\Afi/i then 'fireball'
          end
        end

        def canonical_type!(type)
          canonical_type(type) || raise(ArgumentError, "Invalid type: #{type}")
        end

        def type_name(type)
          case type
          when 'armor' then '<:gow_armor:364140763178729472>'
          when 'life' then '<:gow_life:364140763254226944>'
          when 'attack' then '<:gow_attack:364140763099168769>'
          when 'haste' then '💨'
          when 'magic' then '<:gow_magic:364140762964951051>'
          when 'luck' then '🍀'
          when 'power' then '⚡'
          when 'unlock' then '🆙'
          when 'heroism' then '🦸‍♀️'
          when 'fireball' then '🔥'
          else '❓'
          end
        end

        def canonical_sunglasses(sunglasses)
          case sunglasses
          when '😎', '⚡' then sunglasses
          end
        end

        def boon?(type, power_is_boon)
          case type
          when 'armor', 'life', 'attack', 'magic' then true
          when 'power' then power_is_boon
          else false
          end
        end

        def set_room_types(event, floor, hash)
          return event.message.react('👎') if hash.empty?

          key = key_for(event.channel.id, floor)
          *created, _ = redis.pipelined do |pipeline|
            hash.each { |room, type| pipeline.hset(key, room, type) }
            pipeline.expireat(key, end_of_week.to_i)
          end
          event.message.react(created.all? ? '👍' : '👆')
        end

        def show_room(event, floor, room)
          key = key_for(event.channel.id, floor)
          type = canonical_type(redis.hget(key, room)) || 'unknown'
          event << "Floor #{floor}: #{room_name(room)} = #{type_name(type)}"
        end

        def format_floor_line(floor, map, hide_boon_rooms)
          line = ["Floor #{floor}: "]
          first = true
          hidden = false
          rooms(floor).each do |room|
            type = canonical_type(map[room]) || 'unknown'
            hide = hide_boon_rooms != '😎' && room != 'boss' && boon?(type, hide_boon_rooms != '⚡')
            line << '||' if hide != hidden
            hidden = hide

            line << ', ' unless first
            first = false
            line << room_name(room) << ' = ' << type_name(type)
          end
          line << '||' if hidden
          line.join
        end

        def reset_floor(event, floor)
          key = key_for(event.channel.id, floor)
          redis.del(key)
          event.message.react('👍')
        end

        def show_floor(event, floor, hide_boon_rooms)
          key = key_for(event.channel.id, floor)
          map = redis.hgetall(key)
          event << format_floor_line(floor, map, hide_boon_rooms)
        end

        def show_map(event, hide_boon_rooms)
          key_pattern = key_for(event.channel.id, '*')
          # @type var map: Hash[Integer, Hash[String, String]]
          map = {}
          redis.scan_each(match: key_pattern) do |key|
            # @type var floor: String
            /:(?<floor>\d+)\z/ =~ key or next
            map[floor.to_i] = redis.hgetall(key)
          end
          displayed = false
          map.keys.sort.each do |floor|
            event << format_floor_line(floor, map[floor], hide_boon_rooms)
            displayed = true
          end
          event << 'No map has been set up yet.' unless displayed
        end

        def show_help(event)
          event << 'Please consult <#632096835330113556> for full instructions. Below is a summary.'
          event << ''
          event << 'Show current map: ♒🏰'
          event << 'Show map of one floor: ♒🏰 _floor_'
          event << 'Show a specific room: ♒🏰 _floor_ _room_'
          event << 'Set or update room: ♒🏰 _floor_ _room_ _type_'
          event << ''
          event << '_floor_ should be a number.'
          event << '_room_ should be `ii`, `iii`, `iv`, `v`, or `boss`.'
          event << '_type_ should be `armor`, `life`, `attack`, `haste`, `magic`, `luck`, `power`, `unlock`, `heroism`, or `fireball`; you can abbreviate the type to the first two letters.' # rubocop:disable Layout/LineLength
        end
      end
    end
  end
end
