# frozen_string_literal: true

# SPDX-FileCopyrightText: © 2018, 2019, 2021 Chris Jester-Young <cky@cky.nz>
# SPDX-License-Identifier: AGPL-3.0-or-later

require_relative '../utils/command_container'
require_relative '../typecheck/popen'

module Aquabot
  module Commands
    # Implements various bot control commands.
    module Control
      extend Aquabot::Utils::CommandContainer
      extend Aquabot::Typecheck::Popen

      setup :control, max_args: 0

      command(:🆙,
              description: 'Update code to latest') do |event|
        prev = head
        err, succeeded = git 'pull', '-q', '--ff-only'
        succeeded or next "Failed to pull:\n\n```\n#{err}\n```"
        cur = head
        event << "Code updated from #{prev} to #{cur}."
        if cur != prev
          changes, succeeded = git 'log', '--oneline', '--reverse', "#{prev}..#{cur}"
          event << "\nChanges:\n```\n#{changes}\n```" if succeeded
        end
      end

      command(:🤔,
              description: 'Run git status') do |event|
        status, = git 'status'
        event << "```#{status}```"
      end

      command(:⏹,
              description: 'Request the bot to exit') do |event|
        event.message.react('👋')
        event.bot.stop
        exit
      end

      class << self
        private

        def topdir
          "#{__dir__}/../../.."
        end

        def git(*args)
          Dir.exist?("#{topdir}/.git") or raise("Can't find git directory!")
          output = popen(['/usr/bin/git', '-C', topdir, *args],
                         external_encoding: Encoding::UTF_8,
                         err: %i[child out]).read
          succeeded = Process.last_status&.success? || false
          [output, succeeded]
        end

        def head
          git('show-ref', '-s8', '--head', '--heads', 'HEAD').first&.chomp
        end
      end
    end
  end
end
