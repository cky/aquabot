# frozen_string_literal: true

# SPDX-FileCopyrightText: © 2018, 2019, 2021 Chris Jester-Young <cky@cky.nz>
# SPDX-License-Identifier: AGPL-3.0-or-later

require_relative '../utils/command_container'

module Aquabot
  module Commands
    # A replacement help command that lists only commands accessible
    # to the originating channel. Code largely based on discordrb's
    # own help command.
    module Help
      extend Aquabot::Utils::CommandContainer

      setup :help, channels: []

      command(:❓,
              aliases: %i[❔],
              description: 'List valid commands or show help for given command',
              max_args: 1) do |event, command_name|
        bot = Aquabot::Bot.cast!(event.bot)
        channel = event.channel

        if command_name
          command = bot.commands[command_name.to_sym]
          command = command.aliased_command if command.is_a?(Discordrb::Commands::CommandAlias)
          unless command && bot.command_accessible?(channel: channel, command: command)
            next "The command `#{command_name}` does not exist!"
          end

          show_help(event, command: command)
        else
          list_commands(event, channel: channel, globals: bot.global_commands,
                               locals: bot.local_commands(channel: channel))
        end
      end

      class << self
        private

        def show_help(event, command:)
          usage = command.attributes[:usage]
          parameters = command.attributes[:parameters]
          event << "**`#{command.name}`**: #{description(command)}"
          event << "Usage: `#{usage}`" if usage
          return unless parameters

          event << 'Accepted Parameters:'
          event << "```\n#{parameters.join("\n")}\n```"
        end

        def list_commands(event, channel:, globals:, locals:)
          event << '**Global commands:**'
          event << help_text(globals)
          event << "**Commands for <##{channel.id}>:**"
          event << help_text(locals)
        end

        def help_text(commands)
          commands.map do |command|
            "**`#{command.name}`**: #{description(command)}"
          end.join("\n")
        end

        def description(command)
          command.attributes[:description] || '*No description available*'
        end
      end
    end
  end
end
