# frozen_string_literal: true

# SPDX-FileCopyrightText: © 2021 Chris Jester-Young <cky@cky.nz>
# SPDX-License-Identifier: AGPL-3.0-or-later

require 'discordrb/commands/command_bot'
require_relative 'channel_filter'
require_relative 'commands'
require_relative 'events'

module Aquabot
  # A specialisation of `Discordrb::Commands::CommandBot` for
  # AquaBot-specific functionality
  class Bot < Discordrb::Commands::CommandBot
    # @dynamic config
    attr_reader :config

    def self.cast!(bot)
      bot.is_a?(Bot) || raise(ArgumentError, "expected a #{Bot} but got a #{bot.class}")
      bot
    end

    def initialize(config:)
      @config = config
      super(token: config.core.bot_token, client_id: config.core.client_id,
            name: 'AquaBot', ignore_bots: true, prefix: '♒', help_command: false,
            spaces_allowed: true, channels: config.defaults.channels)
      gateway.check_heartbeat_acks = false
      include!(Commands)
      include!(Events)
    end

    # Returns the global channel filter, which is the union of listed
    # channels for all commands with an explicit channel filter. The bot
    # will only process messages from channels within this filter.
    def global_channel_filter
      @global_channel_filter ||=
        ChannelFilter.new(main_commands.flat_map { |command| channels_for(command: command) })
    end

    # Returns a list of commands without an explicit channel filter.
    def global_commands
      @global_commands ||= main_commands do |command|
        channels_for(command: command).empty?
      end.freeze
    end

    # Returns a list of commands with an explicit channel filter, that
    # are accessible to `channel`.
    def local_commands(channel:)
      @local_commands ||= Hash.new do |hash, channel_id|
        hash[channel_id] = main_commands do |command|
          channels = channels_for(command: command)
          channels.any? && channels?(channel(channel_id), channels)
        end
      end
      @local_commands[channel.id]
    end

    # Returns whether `channel` supports `command`.
    def command_accessible?(channel:, command:)
      channels?(channel, channels_for(command: command))
    end

    # Returns which channels support `command`. An empty list means
    # it's a global command.
    def channels_for(command:)
      command.attributes[:channels] || attributes[:channels] || []
    end

    private

    # Returns all non-alias command objects sorted by name, optionally
    # filtered by the given block.
    def main_commands
      commands.each_value.filter_map do |command|
        next unless command.is_a?(Discordrb::Commands::Command)
        next unless !block_given? || yield(command)

        command
      end.sort_by(&:name)
    end

    def channels?(channel, channels)
      return false unless global_channel_filter.match?(channel)
      return true if channels.nil? || channels.empty?

      ChannelFilter[channels].match?(channel)
    end

    # When all your commands and arguments are emoji, no spaces are
    # needed for separating them. Of course, it is convenient to allow
    # spaces too, for ease of tab completion in Discord.
    def simple_execute(chain, event)
      chain.gsub!(/[\s︀-️﻿]*/, '')
      command = chain.slice!(0)
      # @type var args: Array[String]
      args = chain.empty? ? [] : [chain]
      execute_command(command.to_sym, event, args) if command
    end
  end
end
